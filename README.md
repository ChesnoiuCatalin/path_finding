# Path-Finding
A python visualization of the A* path finding algorithm. It allows you to pick your start and end location and view the process of finding the shortest path.
# Specification
- Point format (x,y) (Represent the start and end location)
- ”x” values between 1 and 48
- ”y” values between 1 and 48
- If "Show Steps" is selected, the program will slowly display the algorithm . (step-by-step)
- After pressing the ”Submit” button, the user can set obstacles using the mouse .
- To start the path finding algorithm, the user must press the ”space” button .
- If the algorithm finds no path between points, the program will close and the process will end with exit code 1.
# Requirements
- Python 3.x
- TKinter
- Pygame
# Wath I Learned
- Design in a GUI application using TKinter
- 